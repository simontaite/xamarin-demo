using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Countdown.Core.Android.Source
{
    public static class Time
    {
        /// <summary>
        /// Gets the TimeSpan representing the time between the current system time and specified datetime instance
        /// </summary>
        /// <param name="dt">The DateTime instance to compare to now</param>
        /// <returns>TimeSpan representing the time between now and the datetime specified as an argument</returns>
        public static TimeSpan GetTimeSpan(DateTime dt)
        {
            switch (dt.Kind)
            {
                case DateTimeKind.Utc:
                    return GetTimeSpan(DateTime.UtcNow, dt);
                case DateTimeKind.Unspecified:
                case DateTimeKind.Local:
                    return GetTimeSpan(DateTime.Now, dt);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Gets the timespan represeting the time between the two specified DateTime intances
        /// </summary>
        /// <param name="dt1">The first DateTime instance</param>
        /// <param name="dt2">The first DateTime instance</param>
        /// <returns>TimeSpan represnting the time between the two DateTime instances</returns>
        public static TimeSpan GetTimeSpan(DateTime dt1, DateTime dt2)
        {
            if (dt1 > dt2)
                return dt1 - dt2;

            return dt2 - dt1;
        }
    }
}
