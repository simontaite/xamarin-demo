﻿using System;

namespace Countdown.Core.Android.Source
{
    public static class TimeSpanExtensions
    {
        /// <summary>
        /// Formats a TimeSpan as a human readable string ready for display
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        public static string ToReadableString(this TimeSpan ts)
        {
            string formatted = string.Format("{0}{1}{2}{3}",
                                             ts.Duration().Days > 0
                                                 ? string.Format("{0:0} day{1}, ", ts.Days,
                                                                 ts.Days == 1 ? string.Empty : "s")
                                                 : string.Empty,
                                             ts.Duration().Hours > 0
                                                 ? string.Format("{0:0} hour{1}, ", ts.Hours,
                                                                 ts.Hours == 1 ? string.Empty : "s")
                                                 : string.Empty,
                                             ts.Duration().Minutes > 0
                                                 ? string.Format("{0:0} minute{1}, ", ts.Minutes,
                                                                 ts.Minutes == 1 ? string.Empty : "s")
                                                 : string.Empty,
                                             ts.Duration().Seconds > 0
                                                 ? string.Format("{0:0} second{1}", ts.Seconds,
                                                                 ts.Seconds == 1 ? string.Empty : "s")
                                                 : string.Empty);

            if (formatted.EndsWith(", "))
                formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted))
                formatted = "0 seconds";

            return formatted;
        }
    }
}
