﻿using System;
using System.Timers;

using Android.App;
using Android.OS;
using Android.Widget;

using Countdown.Core.Android.Source;

namespace Countdown
{
    [Activity(Label = "@string/ApplicationName", MainLauncher = true, Icon = "@drawable/icon")]
    public class Activity1 : Activity
    {
        private const int DATE_PICKER = 0x0;
        private const int TIME_PICKER = 0x1;

        private int _counter;
        private DateTime _target;
        private Timer _timer;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            _target = DateTime.Now;

            // Get our button from the layout resource,
            // and attach an event to it
            var pickTime = FindViewById<Button>(Resource.Id.PickTime);
            pickTime.Click += ChooseCountdownTarget;

            var startCountdown = FindViewById<Button>(Resource.Id.StartCountdown);
            startCountdown.Click += BeginCountdown;

            UpdateTarget(false);
        }

        private void BeginCountdown(object sender, EventArgs e)
        {
            UpdateText(Resource.Id.txtTimeRemaining, Resources.GetString(Resource.String.TimeRemaining));

            var totalTime = _target - DateTime.Now;
            _counter = (int)totalTime.TotalSeconds;

            // Call UpdateTimer manually to show text immediately
            UpdateTimer(null, null);

            _timer = new Timer(1000);
            _timer.Elapsed += UpdateTimer;
            _timer.Start();
        }

        private void UpdateText(int id, string text)
        {
            var textView = FindViewById<TextView>(id);
            RunOnUiThread(() => textView.SetText(text, TextView.BufferType.Normal));
        }

        private void UpdateTimer(object sender, ElapsedEventArgs e)
        {
            var ts = TimeSpan.FromSeconds(--_counter);

            if (ts == TimeSpan.Zero) _timer.Stop();

            UpdateText(Resource.Id.DisplayCountdown, ts.ToReadableString());
        }

        private void ChooseCountdownTarget(object sender, EventArgs e)
        {
            ShowDialog(DATE_PICKER);
        }

        protected override Dialog OnCreateDialog(int id)
        {
            switch (id)
            {
                case TIME_PICKER:
                    return new TimePickerDialog(this, OnTimeSet, _target.Hour, _target.Minute, true);
                case DATE_PICKER:
                    return new DatePickerDialog(this, OnDateSet, _target.Year, _target.Month - 1, _target.Day);
                default:
                    return null;
            }
        }

        private void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            // MonthOfYear has 1 added to account for zero based index
            _target = new DateTime(e.Year, e.MonthOfYear + 1, e.DayOfMonth, _target.Hour, _target.Minute, 0);
            ShowDialog(TIME_PICKER);
        }

        private void OnTimeSet(object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            _target = _target.Date.AddHours(e.HourOfDay).AddMinutes(e.Minute);
            UpdateTarget(true);
        }

        private void UpdateTarget(bool check)
        {
            if (check && _target < DateTime.Now)
            {
                AlertDialog alert = new AlertDialog.Builder(this).Create();
                alert.SetTitle("Error");
                alert.SetMessage("Countdown time cannot be in the past");
                alert.SetButton("OK", (sender, args) => { });
                alert.Show();
                _target = DateTime.Now;
            }

            var chosenTarget = FindViewById<TextView>(Resource.Id.ChosenDateTime);
            chosenTarget.Text = _target.ToString("HH:mm, dd MMM yyyy");
        }
    }
}